Create a simple project using React, Redux, Typescript.

Should be the form for user information that can be shown/hidden by clicking on button.
Should be the list of users displayed below the form. The pagination should be done.
Users should be displayed in Ascending order based on LastName, if the LastName is the same then on FirstName, then on age.
Each user info should have a button to edit fields and a button to delete the user.
List of users should be saved in Local Storage.

The functionality can be modified while creating the application.
